Advanced Sanitize module provides ability to replace personal/private data \
with values that are friendly for testing or local development purposes.

### Dependencies
- [FakerPHP](https://github.com/FakerPHP/Faker);

### Setup notes
To sanitize your data you need:

1. Install module;
2. Create a config .yml file to scope and set methods to use \
within sanitization process;
3. Place config file somewhere inside Drupal root;
4. Set filepath to config file at /admin/config/system/sanitize-settings;
5. Start sanitizing either from settings form or with Drush command;

You can set value replacement with use of Faker library, \
with your custom SQL command that will be applied to database directly \
or with some constant value.

To skip some entities/values from processing, \
declare them on field/column basis using \
ignore_entity_id (specific to processing with Entity API)\
or ignore_field_value configuration fields.

It is possible to adjust locale of Faker instance per configuration of field.\
String provided in 'locale' configuration property will be passed as\
locale parameter during Faker instance creation.\
Will work only when config field's data_provider is set to 'faker'.

You can customize sanitization process with use of \
events dispatched during it's run.

### Examples
Look at advanced_sanitize/examples/advanced_sanitize.config.sample.yml \
for example of sanitization config file.
