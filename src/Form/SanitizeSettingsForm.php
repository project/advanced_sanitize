<?php

namespace Drupal\advanced_sanitize\Form;

use Drupal\advanced_sanitize\AdvancedSanitizeInterface;
use Drupal\advanced_sanitize\AdvancedSanitizeService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Advanced Sanitize settings for this site.
 */
class SanitizeSettingsForm extends ConfigFormBase implements AdvancedSanitizeInterface {


  /**
   * The accounts sync service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Advanced sanitize service.
   *
   * @var \Drupal\advanced_sanitize\AdvancedSanitizeService
   */
  protected $advancedSanitizeService;

  /**
   * Constructs a CustomerSyncForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\advanced_sanitize\AdvancedSanitizeService $advanced_sanitize
   *   Advanced sanitize service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    FileSystemInterface $file_system,
    AdvancedSanitizeService $advanced_sanitize,
  ) {
    parent::__construct($configFactory);
    $this->fileSystem = $file_system;
    $this->advancedSanitizeService = $advanced_sanitize;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('advanced_sanitize.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advanced_sanitize_sanitize_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['advanced_sanitize.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['config_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to config'),
      '#description' => $this->t('Specify path to sanitize config file relative to Drupal root.'),
      '#default_value' => $this->config('advanced_sanitize.settings')->get('config_path'),
    ];

    $form['batch_entity_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity batch limit'),
      '#description' => $this->t('Maximum amount of entities processed in one batch.'),
      '#default_value' => $this->config('advanced_sanitize.settings')->get('batch_entity_limit') ?? AdvancedSanitizeInterface::LIMIT,
      '#min' => 1,
    ];

    $form['batch_sql_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('SQL batch limit'),
      '#description' => $this->t('Maximum amount of database queries processed in one batch.'),
      '#default_value' => $this->config('advanced_sanitize.settings')->get('batch_sql_limit') ?? AdvancedSanitizeInterface::LIMIT,
      '#min' => 1,
    ];
    $form['actions']['sanitize'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sanitize'),
      '#submit' => ['::launchSanitize'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (
      empty($form_state->getValue('config_path')) ||
      !$this->fileSystem->realpath(DRUPAL_ROOT . '/' . $form_state->getValue('config_path'))
    ) {
      $form_state->setErrorByName('config_path', $this->t('Found no config by given path!'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('advanced_sanitize.settings')
      ->set('config_path', $form_state->getValue('config_path'));
    $this->config('advanced_sanitize.settings')
      ->set('batch_entity_limit', (int) $form_state->getValue('batch_entity_limit'));
    $this->config('advanced_sanitize.settings')
      ->set('batch_sql_limit', (int) $form_state->getValue('batch_sql_limit'));
    $this->config('advanced_sanitize.settings')
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function launchSanitize(array &$form, FormStateInterface $form_state) {
    if (!empty($this->config('advanced_sanitize.settings')
      ->get('config_path'))) {
      $this->advancedSanitizeService->sanitize(FALSE);
    }
    else {
      $this->messenger()->addWarning('Path to sanitize configuration file is not set!');
    }
  }

}
