<?php

namespace Drupal\advanced_sanitize;

/**
 * Module-wide constants.
 */
interface AdvancedSanitizeInterface {

  /**
   * Singular batch operation item limit.
   *
   * @var int
   */
  public const LIMIT = 100;

}
