<?php

namespace Drupal\advanced_sanitize\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the event being fired upon batch processing finish.
 */
class FinishedSanitizeEvent extends Event {

  /**
   * TRUE if batch successfully completed.
   *
   * @var bool
   */
  protected bool $success;

  /**
   * Batch results.
   *
   * @var array
   */
  protected array $results;

  /**
   * An array of function calls.
   *
   * @var array
   */
  protected array $operations;

  /**
   * Constructs a new PostSanitizeEvent object.
   *
   * @param bool $success
   *   TRUE if batch successfully completed.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   An array of function calls.
   */
  public function __construct(bool $success, array $results, array $operations) {
    $this->success = $success;
    $this->results = $results;
    $this->operations = $operations;
  }

  /**
   * Retrieve batch success status.
   *
   * @return bool
   *   TRUE if batch successfully completed.
   */
  public function getSuccess(): bool {
    return $this->success;
  }

  /**
   * Returns batch results.
   *
   * @return array
   *   Batch results.
   */
  public function getResults(): array {
    return $this->results;
  }

  /**
   * Returns an array of function calls.
   *
   * @return array
   *   An array of function calls.
   */
  public function getOperations(): array {
    return $this->operations;
  }

}
