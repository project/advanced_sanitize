<?php

namespace Drupal\advanced_sanitize\Event;

/**
 * Advanced sanitize events definitions.
 */
final class AdvancedSanitizeEvents {

  /**
   * Name of the event fired at very start of sanitization process.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\StartedSanitizeEvent
   */
  const STARTED_SANITIZE = 'advanced_sanitize.process.started';

  /**
   * Name of event fired right before config definition being put into batch.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\PreprocessDefinitionEvent
   */
  const PREPROCESS_DEFINITION = 'advanced_sanitize.process.preprocess_definition';

  /**
   * Name of the event fired before entity sanitization.
   *
   * Fired before entity data change starts.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\PreSanitizeEvent
   */
  const PRE_SANITIZE = 'advanced_sanitize.entity.pre_sanitize';

  /**
   * Name of the event fired after entity sanitization.
   *
   * Fired after sanitized entity save.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\PostSanitizeEvent
   */
  const POST_SANITIZE = 'advanced_sanitize.entity.post_sanitize';

  /**
   * Name of the event fired before entity revision sanitization.
   *
   * Fired before revision data change starts.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\PreSanitizeEvent
   */
  const PRE_SANITIZE_REVISION = 'advanced_sanitize.revision.pre_sanitize';

  /**
   * Name of the event fired after entity revision sanitization.
   *
   * Fired after sanitized revision save.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\PostSanitizeEvent
   */
  const POST_SANITIZE_REVISION = 'advanced_sanitize.revision.post_sanitize';

  /**
   * Name of the event fired from batch finish callback.
   *
   * @Event
   *
   * @see \Drupal\advanced_sanitize\Event\FinishedSanitizeEvent
   */
  const FINISHED_SANITIZE = 'advanced_sanitize.process.finished';

}
