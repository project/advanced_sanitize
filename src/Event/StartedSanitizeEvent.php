<?php

namespace Drupal\advanced_sanitize\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Batch\BatchBuilder;

/**
 * Defines the event being fired right before batch setup.
 */
class StartedSanitizeEvent extends Event {

  /**
   * Validated configs to be passed into sanitization.
   *
   * @var array
   */
  protected array $sanitizeConfigs;

  /**
   * Batch builder object to be set for processing.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected BatchBuilder $batchBuilder;

  /**
   * Whether sanitization will be run in background or not.
   *
   * @var bool
   */
  protected bool $inBackground;

  /**
   * Constructs a new StartedSanitizeEvent object.
   *
   * @param array $sanitizeConfigs
   *   Sanitization configs.
   * @param \Drupal\Core\Batch\BatchBuilder $batchBuilder
   *   Sanitization configs.
   * @param bool $inBackground
   *   An array of function calls.
   */
  public function __construct(array &$sanitizeConfigs, BatchBuilder $batchBuilder, bool $inBackground) {
    $this->sanitizeConfigs = &$sanitizeConfigs;
    $this->batchBuilder = $batchBuilder;
    $this->inBackground = $inBackground;
  }

  /**
   * Get validated configs to be passed into sanitization.
   *
   * Call it like &$event->getSanitizeConfigs() to change original array.
   *
   * @return array
   *   Sanitize config definitions.
   */
  public function &getSanitizeConfigs(): array {
    return $this->sanitizeConfigs;
  }

  /**
   * Batch builder object to be set for processing.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   Batch builder object.
   */
  public function getBatchBuilder(): BatchBuilder {
    return $this->batchBuilder;
  }

  /**
   * Indicates whether sanitization will be run in background or not.
   *
   * @return bool
   *   True when sanitization run in background.
   */
  public function getInBackground(): bool {
    return $this->inBackground;
  }

}
