<?php

namespace Drupal\advanced_sanitize\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the event being fired before config definition being put into batch.
 */
class PreprocessDefinitionEvent extends Event {

  /**
   * Reference to config definition to be put into batch.
   *
   * @var array
   */
  protected array $sanitizeConfig;

  /**
   * Constructs a new PreprocessDefinitionEvent object.
   *
   * @param array $sanitizeConfig
   *   Reference to sanitization config.
   */
  public function __construct(array &$sanitizeConfig) {
    $this->sanitizeConfig = &$sanitizeConfig;
  }

  /**
   * Get reference to single config definition to be put into batch.
   *
   * Call it like &$event->getSanitizeConfig() to change original array.
   *
   * @return array
   *   Sanitize config definition.
   */
  public function &getSanitizeConfig(): array {
    return $this->sanitizeConfig;
  }

}
