<?php

namespace Drupal\advanced_sanitize\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the entity post-sanitize event.
 */
class PostSanitizeRevisionEvent extends Event {

  /**
   * Entity revision being sanitized.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a new PostSanitizeEvent object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity revision being sanitized.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Gets the sanitized entity revision.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity revision being sanitized.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
