<?php

namespace Drupal\advanced_sanitize\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the entity revision pre-sanitize event.
 */
class PreSanitizeRevisionEvent extends Event {

  /**
   * Entity revision being sanitized.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a new PreSanitizeRevisionEvent object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity revision being sanitized.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Gets the entity revision being sanitized.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity revision being sanitized.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
