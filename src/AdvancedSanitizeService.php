<?php

namespace Drupal\advanced_sanitize;

use Drupal\advanced_sanitize\Event\AdvancedSanitizeEvents;
use Drupal\advanced_sanitize\Event\FinishedSanitizeEvent;
use Drupal\advanced_sanitize\Event\PostSanitizeEvent;
use Drupal\advanced_sanitize\Event\PostSanitizeRevisionEvent;
use Drupal\advanced_sanitize\Event\PreprocessDefinitionEvent;
use Drupal\advanced_sanitize\Event\PreSanitizeEvent;
use Drupal\advanced_sanitize\Event\PreSanitizeRevisionEvent;
use Drupal\advanced_sanitize\Event\StartedSanitizeEvent;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Service description.
 */
class AdvancedSanitizeService implements AdvancedSanitizeInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * Default bundle id.
   *
   * @var string
   */
  protected const PLACEHOLDER_BUNDLE = 'default';

  /**
   * Processor ids being recognized by module.
   *
   * @var string
   */
  protected const ALLOWED_DATA_PROVIDERS = ['faker', 'constant', 'sql'];

  /**
   * Default locale to be set for Faker instance.
   *
   * @var string
   */
  protected const DEFAULT_LOCALE = 'en_US';

  /**
   * Faker data provider id.
   *
   * @var string
   */
  protected const DATA_PROVIDER_FAKER = 'faker';

  /**
   * Faker factory.
   *
   * @var \Faker\Generator
   */
  protected Generator $faker;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Advanced sanitize configs.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new AdvancedSanitizeService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundle_info
   *   Bundle info.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    EntityFieldManagerInterface $entity_field_manager,
    ModuleHandlerInterface $module_handler,
    EntityTypeBundleInfo $bundle_info,
    LoggerChannelFactoryInterface $logger,
    EventDispatcherInterface $event_dispatcher,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->moduleHandler = $module_handler;
    $this->bundleInfo = $bundle_info;
    $this->logger = $logger->get('advanced_sanitize');
    $this->config = $config_factory->get('advanced_sanitize.settings');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Discovers and validates sanitize config.
   *
   * @return array
   *   Definitions array to process.
   */
  protected function retrieveConfig(): array {
    $realpath = $this->config->get('config_path');
    if (!empty($realpath)) {
      $sanitizeConfig = Yaml::decode(file_get_contents(DRUPAL_ROOT . '/' . $realpath));
      foreach ($sanitizeConfig as $key => $sanitizeRecord) {
        // Checking "sql" records.
        if (array_key_exists('table_name', $sanitizeRecord) &&
          !array_key_exists('entity_id', $sanitizeRecord)) {
          $this->checkSqlRecord($sanitizeRecord, $sanitizeConfig, $key);
        }
        // Checking "entity" records.
        if (array_key_exists('entity_id', $sanitizeRecord) &&
          !array_key_exists('table_name', $sanitizeRecord)) {
          $this->checkEntityRecord($sanitizeRecord, $sanitizeConfig, $key);
        }

      }
      return array_filter($sanitizeConfig);
    }
    return [];
  }

  /**
   * Checks sql-related sanitize record before processing.
   */
  protected function checkSqlRecord(array $sqlRecord, array &$sanitizeConfig, int $key): void {
    if (!array_key_exists('column_mapping', $sqlRecord)) {
      $this->logger->warning('Sql record %key contains not enough column data!', [
        '@key' => $key,
      ]);
      unset($sanitizeConfig[$key]);
    }
    if (!array_key_exists('unique_column', $sqlRecord)) {
      $this->logger->warning("Sql record %key must contain table's unique column identifier!", [
        '@key' => $key,
      ]);
      unset($sanitizeConfig[$key]);
    }
  }

  /**
   * Checks entity-related sanitize record before processing.
   *
   * @param array $entitySanitizeRecord
   *   Single sanitize record.
   * @param array $sanitizeConfig
   *   Sanitize configuration.
   * @param int $key
   *   Record id within sanitize config.
   */
  protected function checkEntityRecord(array $entitySanitizeRecord, array &$sanitizeConfig, int $key): void {
    $entityId = $entitySanitizeRecord['entity_id'];
    $bundleId = $entitySanitizeRecord['bundle_id'];
    $fieldMapping = $entitySanitizeRecord['field_mapping'];
    if (!empty($fieldMapping)) {
      // Load the field definitions for a bundle.
      if ($bundleId === self::PLACEHOLDER_BUNDLE) {
        $bundleFields = $this->entityFieldManager->getFieldStorageDefinitions($entityId);
      }
      else {
        $bundleFields = $this->entityFieldManager->getFieldDefinitions($entityId, $bundleId);
      }
      $bundleFields = array_keys($bundleFields);

      // Check field mapping.
      foreach ($fieldMapping as $fieldKey => $fieldDefinition) {
        // Verify that field exists within entity.
        $fieldId = $fieldDefinition['field_id'];
        if (!in_array($fieldId, $bundleFields, TRUE)) {
          $this->logger->warning('Field @field_id was not found within @entity_id entity definition!', [
            '@field_id' => $fieldId,
            '@entity_id' => $entityId,
          ]);
          unset($sanitizeConfig[$key]['field_mapping'][$fieldKey]);
        }
        // Validate data providers.
        if (!in_array($fieldDefinition['data_provider'], self::ALLOWED_DATA_PROVIDERS, TRUE)) {
          $this->logger->warning('Unsupported data provider: @processor', [
            '@processor' => $fieldDefinition['data_provider'],
          ]);
          // Remove config entry of not-supported data providers.
          unset($sanitizeConfig[$key]['field_mapping'][$fieldKey]);
        }
      }
    }
    else {
      $this->logger->warning('Sanitize configuration is empty for @entity entity (@bundle bundle)!', [
        '@entity' => $entityId,
        '@bundle' => $bundleId,
      ]);
      unset($sanitizeConfig[$key]);
    }
  }

  /**
   * Sets up batch processing.
   *
   * @param bool $runInBackground
   *   With TRUE sets batch for launch from backend.
   */
  public function sanitize(bool $runInBackground = TRUE): void {
    $validDefinitions = $this->retrieveConfig();
    if (!empty($validDefinitions)) {
      $batchBuilder = $this->setBatchBuilder($validDefinitions);

      $event = new StartedSanitizeEvent($validDefinitions, $batchBuilder, $runInBackground);
      $this->eventDispatcher->dispatch($event, AdvancedSanitizeEvents::STARTED_SANITIZE);

      batch_set($batchBuilder->toArray());

      if ($runInBackground) {
        drush_backend_batch_process();
      }
    }
  }

  /**
   * Sets the batch builder.
   *
   * @param array $validDefinitions
   *   Sanitize definitions to process.
   */
  public function setBatchBuilder(array $validDefinitions): BatchBuilder {
    $batchBuilder = new BatchBuilder();
    $batchBuilder->setTitle($this->t('Processing...'))
      ->setInitMessage($this->t('Initializing...'))
      ->setProgressMessage($this->t('Sanitized @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'))
      ->setFinishCallback([$this, 'finishBatch']);

    $limit = AdvancedSanitizeInterface::LIMIT;

    foreach ($validDefinitions as $definition) {
      $event = new PreprocessDefinitionEvent($definition);
      $this->eventDispatcher->dispatch($event, AdvancedSanitizeEvents::PREPROCESS_DEFINITION);

      if (array_key_exists('table_name', $definition)) {
        $select = $this->database->select($definition['table_name'], 'table');
        $select->addField('table', $definition['unique_column']);
        if (!empty($definition['where'])) {
          $select->where($definition['where'], $definition['where_args'] ?? []);
        }
        $tableKeys = $select->execute()->fetchCol();
        if (!empty($tableKeys)) {
          if ($this->config->get('batch_sql_limit')) {
            $limit = $this->config->get('batch_sql_limit');
          }
          $chunks = array_chunk($tableKeys, $limit);
          $count = count($chunks);
          foreach ($chunks as $i => $chunk) {
            $batchBuilder->addOperation([$this, 'processBatchSql'], [$chunk, (int) $count, $i + 1, $definition]);
          }
        }
      }

      if (array_key_exists('entity_id', $definition)) {
        $query = $this->entityTypeManager->getStorage($definition['entity_id'])->getQuery();
        if ($definition['bundle_id'] !== self::PLACEHOLDER_BUNDLE) {
          $query->condition('type', $definition['bundle_id']);
        }
        $query->accessCheck(FALSE);
        $entityIds = $query->execute();
        if (!empty($entityIds)) {
          if ($this->config->get('batch_entity_limit')) {
            $limit = $this->config->get('batch_entity_limit');
          }
          $chunks = array_chunk($entityIds, $limit);
          $count = count($chunks);
          foreach ($chunks as $i => $chunk) {
            $batchBuilder->addOperation([$this, 'processBatchDrupal'], [$chunk, (int) $count, $i + 1, $definition]);
          }
        }
      }
    }

    return $batchBuilder;
  }

  /**
   * Process batched SQL operation.
   *
   * @param array $chunk
   *   Entities chunk.
   * @param int $count
   *   The total chunks count.
   * @param int $i
   *   The current chunk index.
   * @param array $definition
   *   The config definition.
   * @param array|object $context
   *   Batch context.
   */
  public function processBatchSql(array $chunk, int $count, int $i, array $definition, &$context): void {
    $context['message'] = $this->t('Processing @table table: @percent%', [
      '@table' => $definition['table_name'],
      '@percent' => round($i * 100 / $count, 0, PHP_ROUND_HALF_DOWN),
    ]);

    $this->initFaker();

    foreach ($chunk as $recordId) {
      $this->updateRow($definition, $recordId);
    }
  }

  /**
   * Process Drupal entities batch operation.
   *
   * @param array $chunk
   *   Entities chunk.
   * @param int $count
   *   The total chunks count.
   * @param int $i
   *   The current chunk index.
   * @param array $definition
   *   The config definition.
   * @param array|object $context
   *   Batch context.
   */
  public function processBatchDrupal(array $chunk, int $count, int $i, array $definition, &$context): void {
    $context['message'] = $this->t('Processing @type entities: @percent%', [
      '@type' => $definition['entity_id'],
      '@percent' => round($i * 100 / $count, 0, PHP_ROUND_HALF_DOWN),
    ]);

    $this->initFaker();

    $entities = $this->entityTypeManager->getStorage($definition['entity_id'])->loadMultiple($chunk);
    foreach ($entities as $entity) {
      $this->processWithDrupal($entity, $definition);
    }
  }

  /**
   * Handles field data sanitization with Faker.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity being sanitized.
   * @param array $sanitizeRecord
   *   Sanitize config record related to entity.
   */
  public function processWithDrupal(EntityInterface $entity, array $sanitizeRecord): void {
    if ($entity instanceof FieldableEntityInterface) {
      $save = FALSE;
      foreach ($sanitizeRecord['field_mapping'] as $fieldMapping) {
        $event = new PreSanitizeEvent($entity);
        $this->eventDispatcher->dispatch($event, AdvancedSanitizeEvents::PRE_SANITIZE);
        // Leave empty fields empty.
        if (!$entity->get($fieldMapping['field_id'])->isEmpty()) {
          // Skip processing for ignored entity_id.
          if (
            !empty($fieldMapping['ignore_entity_id']) &&
            in_array(
              $entity->id(),
              $fieldMapping['ignore_entity_id'],
              FALSE)
          ) {
            continue;
          }

          // Skip processing for ignored field value.
          if (
            !empty($fieldMapping['ignore_field_values']) &&
            in_array(
              $entity->get($fieldMapping['field_id'])->value,
              $fieldMapping['ignore_field_values'],
              FALSE)
          ) {
            continue;
          }

          switch ($fieldMapping['data_provider']) {
            case 'faker':
              $fakeData = $this->getFakerData(
                $fieldMapping['method'],
                $fieldMapping['parameters'] ?? NULL
              );
              if (array_key_exists('ensure_unique', $fieldMapping) && $fieldMapping['ensure_unique']) {
                $fakeData = uniqid("", FALSE) . $fakeData;
              }
              $entity->set($fieldMapping['field_id'], $fakeData);
              $save = TRUE;
              break;

            case 'constant':
              $entity->set(
                $fieldMapping['field_id'],
                $fieldMapping['value']
              );
              $save = TRUE;
              break;
          }

          $event = new PostSanitizeEvent($entity);
          $this->eventDispatcher->dispatch($event, AdvancedSanitizeEvents::POST_SANITIZE);

          if ($entity->getEntityType()->isRevisionable()) {
            $this->sanitizeRevisions($entity);
          }
        }
      }
      if ($save) {
        $entity->save();
      }
    }
  }

  /**
   * Handles field data replacement with sql expression.
   *
   * @param array $sanitizeRecord
   *   Processed entity's field mapping.
   * @param int|string $uniqueId
   *   Unique id of record to be updated.
   */
  public function updateRow(array $sanitizeRecord, int|string $uniqueId): void {
    $query = $this->database->update($sanitizeRecord['table_name']);
    $query->condition($sanitizeRecord['unique_column'], $uniqueId);
    $updFields = [];
    foreach ($sanitizeRecord['column_mapping'] as $colSanitizeDefinition) {
      if ($colSanitizeDefinition['data_provider'] === self::DATA_PROVIDER_FAKER) {
        $this->initFaker($colSanitizeDefinition);
      }

      // Do not update ignored record values.
      if (
        !empty($colSanitizeDefinition['ignore_field_values'])
      ) {
        $query->condition(
          $colSanitizeDefinition['col_name'],
          $colSanitizeDefinition['ignore_field_values'],
          'NOT IN'
        );
      }

      if ($colSanitizeDefinition['data_provider'] === 'faker') {
        $fakeValue = $this->getFakerData(
          $colSanitizeDefinition['method'],
          $colSanitizeDefinition['parameters'] ?? NULL
        );
        if (array_key_exists('ensure_unique', $colSanitizeDefinition) && $colSanitizeDefinition['ensure_unique']) {
          $fakeValue = uniqid("", FALSE) . $fakeValue;
        }
        $updFields[$colSanitizeDefinition['col_name']] = $fakeValue;
      }
      if ($colSanitizeDefinition['data_provider'] === 'constant') {
        $updFields[$colSanitizeDefinition['col_name']] =
          $colSanitizeDefinition['value'];
      }
      if ($colSanitizeDefinition['data_provider'] === 'expression') {
        $query
          ->expression(
            $colSanitizeDefinition['col_name'],
            $colSanitizeDefinition['expression'],
            $colSanitizeDefinition['expr_arguments']
          );
      }
    }
    if (!empty($updFields)) {
      $query->fields($updFields);
    }
    $query->execute();
  }

  /**
   * Make all revisions copy given already sanitized entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Sanitized entity.
   */
  protected function sanitizeRevisions(EntityInterface $entity): void {
    $revisionIds = $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->getQuery()
      ->accessCheck()
      ->allRevisions()
      ->condition('type', $entity->getEntityType()->getKey('id'))
      ->execute();

    foreach ($revisionIds as $revisionId) {
      $revision = $this->entityTypeManager->getStorage($entity->getEntityTypeId())->loadRevision($revisionId);

      if (isset($revision)) {
        $event = new PreSanitizeRevisionEvent($revision);
        $this->eventDispatcher->dispatch($event, AdvancedSanitizeEvents::PRE_SANITIZE_REVISION);

        foreach ($entity->getFields() as $name => $field) {
          $revision->set($name, $field->getValue());
        }
        $revision->save();

        $event = new PostSanitizeRevisionEvent($revision);
        $this->eventDispatcher->dispatch($event, AdvancedSanitizeEvents::POST_SANITIZE_REVISION);
      }
    }
  }

  /**
   * Batch finished callback for sanitize operation.
   *
   * @param bool $success
   *   TRUE if batch successfully completed.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   An array of function calls.
   */
  public function finishBatch(bool $success, array $results, array $operations) {
    drupal_flush_all_caches();
    $finishedSanitiseEvent = new FinishedSanitizeEvent($success, $results, $operations);
    $this->eventDispatcher->dispatch($finishedSanitiseEvent, AdvancedSanitizeEvents::FINISHED_SANITIZE);
    if ($success) {
      \Drupal::messenger()->addMessage('Sanitization was successfully completed!');
    }
  }

  /**
   * Initialise Faker instance with config data.
   *
   * @param array $definition
   *   Config definition to retrieve settings from.
   */
  protected function initFaker(array $definition = []): void {
    if (isset($definition['locale'])) {
      $this->faker = Factory::create($definition['locale']);
    }
    else {
      $this->faker = Factory::create(self::DEFAULT_LOCALE);
    }
  }

  /**
   * Retrieve fake data from Faker.
   *
   * @param string $method
   *   Faker method to call.
   * @param array|null $parameters
   *   Parameters to use with method.
   *
   * @return mixed
   *   Data generated by Faker.
   */
  protected function getFakerData(string $method, array|null $parameters = NULL): mixed {
    return $this->faker
      ->unique()
      ->format(
        $method,
        $parameters ?? []
      );
  }

  /**
   * Retrieve instance of Advanced Sanitize config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Advanced sanitize config instance.
   */
  public function config(): ImmutableConfig {
    return $this->config;
  }

}
