<?php

namespace Drupal\advanced_sanitize\Commands;

use Drupal\advanced_sanitize\AdvancedSanitizeService;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * Drush sql-sanitize plugin for sanitizing data.
 *
 * @see \Drush\Drupal\Commands\sql\SanitizeSessionsCommands
 */
class AdvancedSanitizeCommands extends DrushCommands {
  use StringTranslationTrait;

  /**
   * Advanced sanitize service.
   *
   * @var \Drupal\advanced_sanitize\AdvancedSanitizeService
   */
  protected AdvancedSanitizeService $advancedSanitizeService;

  /**
   * Constructs a new AdvancedSanitizeCommands object.
   *
   * @param \Drupal\advanced_sanitize\AdvancedSanitizeService $advancedSanitizeService
   *   Advanced sanitize service.
   */
  public function __construct(AdvancedSanitizeService $advancedSanitizeService) {
    $this->advancedSanitizeService = $advancedSanitizeService;
    parent::__construct();
  }

  /**
   * Standalone data anonymization launch command.
   *
   * @command advanced_sanitize:sanitize
   * @aliases adsan
   * @validate-module-enabled advanced_sanitize
   */
  public function advancedSanitize() {
    if (
      $this->advancedSanitizeService->config()
        ->get('config_path')
    ) {
      $this->advancedSanitizeService->sanitize();
    }
    else {
      $this->logger()?->warning('Path to sanitize configuration file is not set!');
    }
  }

}
